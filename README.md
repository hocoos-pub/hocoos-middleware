# caddy-middleware
## Build
```
xcaddy build --output ./caddy.exe --with gitlab.com/hocoos-pub/hocoos-middleware@v0.1.0
```
## Example
````json
{
  "logging": {
    "logs": {
      "default": {
        "level": "DEBUG"
      }
    }
  },
  "apps": {
    "http": {
      "servers": {
        "myserver": {
          "listen": [":443",":80"],
          "routes": [
            {
              "match": [{"path": ["/*"]}],
              "handle": [
                {
                  "handler": "hocoos_middleware",
                  "redis_url": "redisdb_connection_string",
                  "cache_ttl": 60,
                  "exclude_hosts": "xxx.cafe,xxxxx.com,localhost"
                },
                {
                "handler": "static_response",
                "status_code": "",
                "body": "Hi there!",
                "close": false,
                "abort": false
                }
              ]
            }
          ]
        }
      }
    }
  }
}

````

